﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using System.IO;

namespace OutlookReportExtractor
{
    public partial class ThisAddIn
    {
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            this.Application.NewMail += new Microsoft.Office.Interop.Outlook
                .ApplicationEvents_11_NewMailEventHandler(ThisApplication_NewMail);
        }

        private void ThisApplication_NewMail()
        {
            Outlook.MAPIFolder inBox = this.Application.ActiveExplorer()
                .Session.GetDefaultFolder(Outlook
                .OlDefaultFolders.olFolderInbox);
            Outlook.Items inBoxItems = inBox.Items;
            Outlook.MailItem newEmail = null;
            inBoxItems = inBoxItems.Restrict("[Unread] = true");

            string[] reportIds = { "BSADV316", "BSADV308", "BSADV310" };

            try
            {
                foreach (object collectionItem in inBoxItems)
                {
                    newEmail = collectionItem as Outlook.MailItem;
                    if (newEmail != null)
                    {
                        if (reportIds.Any(newEmail.Body.Contains))
                        {

                            DateTime receivedDate = newEmail.ReceivedTime;
                            string filePath = $@"\\sa.global.prv\fileshare2\Finance\Financeiro-Contabil\Tesouraria\UploadFiles\{reportIds.Where(newEmail.Body.Contains).First()} {receivedDate.ToString("yyyyMMdd")}.txt";

                            if ( ! File.Exists(filePath) || ! File.ReadAllText(filePath).Contains(newEmail.Body) )
                            {
                                StreamWriter sw = null;
                                sw = new StreamWriter(filePath, true);
                                sw.WriteLine(newEmail.Body);
                                sw.Flush();
                                sw.Close();
                            }

                            newEmail.UnRead = false;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorInfo = (string)ex.Message
                    .Substring(0, 11);
                if (errorInfo == "Cannot save")
                {
                    System.Windows.Forms.MessageBox.Show(@"Você deve criar a pasta C:\TestFileSave");
                }
            }
        }


        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            // Note: Outlook no longer raises this event. If you have code that 
            //    must run when Outlook shuts down, see https://go.microsoft.com/fwlink/?LinkId=506785
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
